import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import javax.swing.event.*;

    public class GUIPlayer extends JFrame {
        // Anfang Attribute
        private JTextField tfName = new JTextField();
        private JLabel jLabel1 = new JLabel();
        private JButton bAdd = new JButton();
        private JLabel jLabel2 = new JLabel();
        private JLabel jLabel3 = new JLabel();
        private JLabel jLabel6 = new JLabel();
        private JNumberField nfID = new JNumberField();
        private JNumberField nfCoin= new JNumberField();
        private JButton bDel = new JButton();
        // Ende Attribute

        public GUIPlayer() {
            // Frame-Initialisierung
            super();
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            int frameWidth = 245;
            int frameHeight = 274;
            setSize(frameWidth, frameHeight);
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (d.width - getSize().width) / 2;
            int y = (d.height - getSize().height) / 2;
            setLocation(x, y);
            setTitle("GUIPlayer");
            setResizable(false);
            Container cp = getContentPane();
            cp.setLayout(null);
            // Anfang Komponenten

            tfName.setBounds(80, 32, 70, 20);
            cp.add(tfName);
            jLabel1.setBounds(96, 8, 38, 20);
            jLabel1.setText("Player");
            cp.add(jLabel1);
            bAdd.setBounds(16, 184, 75, 25);
            bAdd.setText("Add");
            bAdd.setMargin(new Insets(2, 2, 2, 2));
            bAdd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    bAdd_ActionPerformed(evt);
                }
            });
            cp.add(bAdd);
            jLabel2.setBounds(32, 56, 44, 19);
            jLabel2.setText("^ID");
            cp.add(jLabel2);
            jLabel3.setBounds(88, 56, 44, 19);
            jLabel3.setText("^Name");
            cp.add(jLabel3);
            jLabel6.setBounds(104, 104, 30, 19);
            jLabel6.setText("Coin");
            cp.add(jLabel6);
            nfID.setBounds(8, 32, 67, 20);
            nfID.setText("");
            cp.add(nfID);
            nfCoin.setBounds(80, 80, 67, 20);
            nfCoin.setText("");
            cp.add(nfCoin);
            bDel.setBounds(136, 184, 75, 25);
            bDel.setText("Delete");
            bDel.setMargin(new Insets(2, 2, 2, 2));
            bDel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    bDel_ActionPerformed(evt);
                }
            });
            cp.add(bDel);
            // Ende Komponenten

            setVisible(true);
        } // end of public GUICard

        // Anfang Methoden

  /*public static void main(String[] args) {
    new GUICard();
  } // end of main */

        public void bAdd_ActionPerformed(ActionEvent evt) {
            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:test.db");
                c.setAutoCommit(false);
                System.out.println("Opened database successfully");

                stmt = c.createStatement();
                String sql = "INSERT INTO PLAYER (ID,NAME,COIN) " +
                        "VALUES ("+nfID.getInt()+", '"+tfName.getText()+"', "+nfCoin.getInt()+");";
                stmt.executeUpdate(sql);

                stmt.close();
                c.commit();
                c.close();
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }
            System.out.println("Records created successfully");
        } // end of bAdd_ActionPerformed

        public void bDel_ActionPerformed(ActionEvent evt) {
            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:test.db");
                c.setAutoCommit(false);
                System.out.println("Opened database successfully");

                stmt = c.createStatement();
                String sql = "DELETE from PLAYER where ID="+nfID.getInt()+";";
                stmt.executeUpdate(sql);
                c.commit();

                stmt.close();
                c.close();
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }
            System.out.println("Operation done successfully");
        } // end of bDel_ActionPerformed

        // Ende Methoden
} // end of class GUICard

