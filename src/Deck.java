public class Deck {
    int deckCount;

    public Deck(int deckCount) {
        this.deckCount = deckCount;
    }
    public void setDeckCount(int deckCount) {
        this.deckCount = deckCount;
    }

    public int getDeckCount() {
        return this.deckCount;
    }
}

