public class Graveyard {
    private int graveCount;

    public Graveyard(int graveCount) {
        this.graveCount = graveCount;
    }

    public void setGraveCount(int graveCount) {
        this.graveCount = graveCount;
    }
    public int getGraveCount() {
        return this.graveCount;
    }
}
