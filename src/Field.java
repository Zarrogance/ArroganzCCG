
public class Field {
    private boolean[] fieldOccupied = new boolean[2];
    private Hand handP1 = new Hand(5);
    private Hand handP2 = new Hand(5);
    private Deck deckP1 = new Deck(30);
    private Deck deckP2 = new Deck(30);
    private Graveyard graveP1 = new Graveyard(0);
    private Graveyard graveP2 = new Graveyard(0);

    public boolean getFieldOccupy(int i) {
        return this.fieldOccupied[i];
    }
    public void setFieldOccupy(int i, boolean state) {
        this.fieldOccupied[i] = state;
    }

    public void drawCard(int amount) {

        deckP1.setDeckCount(deckP1.getDeckCount() - amount);
        int diff = handP1.getHandCount() + amount - 9;
        handP1.setHandCount(handP1.getHandCount() + amount);
        if (diff > 0) {
            //for (int i = 0; i < diff; i++) {
               handP1.setHandCount(handP1.getHandCount()-diff);
               graveP1.setGraveCount(graveP1.getGraveCount()+diff);
           // }
        }
        System.out.println(handP1.getHandCount());
        System.out.println(deckP1.getDeckCount());
        System.out.println(graveP1.getGraveCount());
        System.out.println(diff);

    }
}
