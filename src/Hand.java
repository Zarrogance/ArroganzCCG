public class Hand    {
    int handCount;

    public Hand(int handCount) {
        this.handCount = handCount;
    }
    public void setHandCount(int handCount) {
        this.handCount = handCount;
    }
    public int getHandCount() {
        return this.handCount;
    }
}
