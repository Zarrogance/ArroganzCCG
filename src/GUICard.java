import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import javax.swing.event.*;

public class GUICard extends JFrame {
  // Anfang Attribute
  private JTextField tfName = new JTextField();
  private JTextField tfJob = new JTextField();
  private JTextField tfRare = new JTextField();
  private JLabel jLabel1 = new JLabel();
  private JButton bAdd = new JButton();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JLabel jLabel5 = new JLabel();
  private JLabel jLabel6 = new JLabel();
  private JLabel jLabel7 = new JLabel();
  private JNumberField nfID = new JNumberField();
  private JNumberField nfATK = new JNumberField();
  private JNumberField nfHP = new JNumberField();
  private JNumberField nfCost = new JNumberField();
  private JNumberField nfPrice = new JNumberField();
  private JLabel jLabel8 = new JLabel();
  private JLabel jLabel9 = new JLabel();
  private JButton bDel = new JButton();
  // Ende Attribute
  
  public GUICard() {
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 245; 
    int frameHeight = 274;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("GUICard");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    tfName.setBounds(80, 32, 70, 20);
    cp.add(tfName);
    tfJob.setBounds(152, 32, 70, 20);
    cp.add(tfJob);
    tfRare.setBounds(8, 80, 70, 20);
    cp.add(tfRare);
    jLabel1.setBounds(96, 8, 38, 20);
    jLabel1.setText("Cards");
    cp.add(jLabel1);
    bAdd.setBounds(16, 184, 75, 25);
    bAdd.setText("Add");
    bAdd.setMargin(new Insets(2, 2, 2, 2));
    bAdd.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bAdd_ActionPerformed(evt);
      }
    });
    cp.add(bAdd);
    jLabel2.setBounds(32, 56, 44, 19);
    jLabel2.setText("^ID");
    cp.add(jLabel2);
    jLabel3.setBounds(88, 56, 44, 19);
    jLabel3.setText("^Name");
    cp.add(jLabel3);
    jLabel4.setBounds(176, 56, 32, 19);
    jLabel4.setText("^Job");
    cp.add(jLabel4);
    jLabel5.setBounds(32, 104, 38, 19);
    jLabel5.setText("^Rare");
    cp.add(jLabel5);
    jLabel6.setBounds(104, 104, 30, 19);
    jLabel6.setText("ATK");
    cp.add(jLabel6);
    jLabel7.setBounds(176, 104, 30, 19);
    jLabel7.setText("HP");
    cp.add(jLabel7);
    nfID.setBounds(8, 32, 67, 20);
    nfID.setText("");
    cp.add(nfID);
    nfATK.setBounds(80, 80, 67, 20);
    nfATK.setText("");
    cp.add(nfATK);
    nfHP.setBounds(152, 80, 67, 20);
    nfHP.setText("");
    cp.add(nfHP);
    nfCost.setBounds(40, 128, 67, 20);
    nfCost.setText("");
    cp.add(nfCost);
    nfPrice.setBounds(112, 128, 67, 20);
    nfPrice.setText("");
    cp.add(nfPrice);
    jLabel8.setBounds(48, 152, 38, 19);
    jLabel8.setText("^Cost");
    cp.add(jLabel8);
    jLabel9.setBounds(120, 152, 41, 19);
    jLabel9.setText("^Price");
    cp.add(jLabel9);
    bDel.setBounds(136, 184, 75, 25);
    bDel.setText("Delete");
    bDel.setMargin(new Insets(2, 2, 2, 2));
    bDel.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bDel_ActionPerformed(evt);
      }
    });
    cp.add(bDel);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public GUICard
  
  // Anfang Methoden
  
  /*public static void main(String[] args) {
    new GUICard();
  } // end of main */
  
  public void bAdd_ActionPerformed(ActionEvent evt) {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:test.db");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "INSERT INTO CARDS (ID,NAME,JOB,RARE,ATK,HP,COST,PRICE) " +
              "VALUES ("+nfID.getInt()+", '"+tfName.getText()+"', '"+tfJob.getText()+"', '"+tfRare.getText()+"', "+nfATK.getInt()+", "+nfHP.getInt()+", "+nfCost.getInt()+", "+nfPrice.getInt()+" );";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Records created successfully");
  } // end of bAdd_ActionPerformed

  public void bDel_ActionPerformed(ActionEvent evt) {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:test.db");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "DELETE from CARDS where ID="+nfID.getInt()+";";
      stmt.executeUpdate(sql);
      c.commit();

      stmt.close();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Operation done successfully");
  } // end of bDel_ActionPerformed

  // Ende Methoden
} // end of class GUICard
